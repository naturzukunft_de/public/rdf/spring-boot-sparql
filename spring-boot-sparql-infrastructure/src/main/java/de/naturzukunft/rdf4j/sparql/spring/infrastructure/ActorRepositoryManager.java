package de.naturzukunft.rdf4j.sparql.spring.infrastructure;

import org.eclipse.rdf4j.repository.Repository;

/**
 * Each actor has it's own repository, {@link ActorRepositoryManager} manages this repositories.  
 */
public interface ActorRepositoryManager {
	Repository getRepository(String actorId);
}