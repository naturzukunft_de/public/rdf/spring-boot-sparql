package de.naturzukunft.rdf4j.sparql.spring.controller;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import de.naturzukunft.rdf4j.sparql.spring.domain.SparqlQueryEvaluator;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;


@RestController
@Slf4j
public class SparqlController {

	@Autowired
	private SparqlQueryEvaluator sparqlQueryEvaluator;

	@Autowired
	public SparqlController(SparqlQueryEvaluator sparqlQueryEvaluator) {
		this.sparqlQueryEvaluator = sparqlQueryEvaluator;
	}

	public Mono<ServerResponse> sparqlGet(ServerRequest request) {
		String actorId = request.pathVariable("actorId");
		String query = request.queryParam("query").orElseThrow(()->new RuntimeException("query is mandatory"));
		String acceptHeader = getAcceptHeader(request);
		String defaultGraphUri = request.queryParam("default-graph-uri").orElse(null);
		String namedGraphUri = request.queryParam("named-graph-uri").orElse(null);
		String result = doSparql(actorId, request, query, acceptHeader, defaultGraphUri, namedGraphUri);
		return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(BodyInserters.fromValue(result));
	}

	private String getAcceptHeader(ServerRequest request) {
		return request.headers().accept().stream()
				.map(MediaType::toString)
				.collect(Collectors.joining(","));
	}

	@RequestMapping(value = "/")
	public void test() throws IOException {
		System.out.println("test called");
	}

	private String doSparql(String actorId, ServerRequest request, String query, String acceptHeader, String defaultGraphUri,
			String namedGraphUri) {
		try {
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			sparqlQueryEvaluator.evaluate(actorId, query, acceptHeader, defaultGraphUri, namedGraphUri, os);
			return os.toString();
		} catch (RuntimeException re) {
			log.error("error while evaluating query", re);
			throw new RuntimeException("error while evaluating query", re);
		} 
	}
}