package de.naturzukunft.rdf4j.sparql.spring.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {
		"de.naturzukunft.rdf4j.sparql.spring.infrastructure", 
		"de.naturzukunft.rdf4j.sparql.spring.controller"})
public class SpringBootApp {
	public static void main(String[] args) {
		SpringApplication.run(SpringBootApp.class, args);
	}
}
