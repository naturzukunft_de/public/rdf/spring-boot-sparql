package de.naturzukunft.rdf4j.sparql.spring.controller;

import java.util.Collections;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.impl.client.HttpClients;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;
import org.eclipse.rdf4j.repository.util.Repositories;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

@SpringBootTest(classes=TestApp.class, webEnvironment = WebEnvironment.DEFINED_PORT)
public class TestSparqlRepository {
	
	@Test
	void testSparql() {
		String query = "SELECT * WHERE { ?s ?p ?o }";
		String url = "http://localhost:8080/actors/0815/sparql";
		List<BindingSet> result = sparqlSelect(Values.iri(url), query);
		System.out.println("Now we  except a list of BindingSets:");
		for (BindingSet bindingSet : result) {
			bindingSet.forEach(binding->System.out.println("bindingSet: " + binding.getName() + " - " + binding.getValue()));
		}
	}

	private List<BindingSet> sparqlSelect(IRI url, String query) {
		List<Header> headers = Collections.emptyList(); // Empty collection, because in this test scenario we do not use Oauth2 
		org.apache.http.client.HttpClient client = HttpClients.custom().setDefaultHeaders(headers).build();
		SPARQLRepository repo = new SPARQLRepository(url.stringValue());
		repo.setHttpClient(client);
		return Repositories.tupleQuery(repo, query, r -> QueryResults.asList(r));
	}
}
